import pymysql
import pandas as pd
import json
import sqlalchemy
from dotenv import load_dotenv
import os 
from os.path import join, dirname

class Connection:    
    host='localhost'
    user='root'
    passwd='root'
    db='tamara'
    
    def __init__(self):
        self._conn = pymysql.connect(host='localhost',user='root',passwd='root',db='tamara')
        self._cursor = self._conn.cursor()
    
    @property
    def connection(self):
        return self._conn
    
    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()
    
    def getOrderEvents(self):
        sql = "select * from order_events"
        return self.query(sql)

    def sqlAchemyConn(self):
        sqlalchemy_conn = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                             format('root','root','localhost','tamara'))
        return sqlalchemy_conn
                
def normalize(records: tuple, db_connection : sqlalchemy.engine.base.Engine):
    print('Normalize Item Lists In Progress...')

    try:
        for e in records:
            #unnest item in column payload in table order_events which has event_name Tamara\Component\Order\Model\Event\OrderWasCreated
            if e[0] != 0 and e[3] == 'Tamara\Component\Order\Model\Event\OrderWasCreated':
                d = json.loads(e[4])
                
                for i,ii in enumerate(d):
                    if not d['risk_assessment']:
                        d['risk_assessment'] = [{'risk_assessment': [{}]}]

                # d['discount'] = json.dumps(d['discount'])
                
                df_item = pd.json_normalize(d, 'items', ['locale','status'
                                                        ,['discount','name'],['discount','amount','amount'],['discount','amount','currency']
                                                        ,'order_id','platform','is_mobile','created_at'
                                                        # ,['tax_amount','amount'],['tax_amount','currency']
                                                        ,'description','merchant_id','country_code'
                                                        ,['merchant_url','cancel']
                                                        ,['merchant_url','failure']
                                                        ,['merchant_url','success']
                                                        ,['merchant_url','notification']
                                                        ,'payment_type'
                                                        # ,['total_amount','amount'],['total_amount','currency']
                                                        ,'merchant_name'
                                                        # ,'risk_assessment'
                                                        ,['shipping_amount','amount'],['shipping_amount','currency']
                                                        ,'order_reference_id'
                                                    ])
                
                df_item.insert(loc=0, column='event_id', value=e[1])
                df_item.to_sql(con=db_connection, name='items', if_exists='append',index=False)  
    except TypeError as e:
        print(e)
        return None

        
def main():
   Conn = Connection()
   normalize(Conn.getOrderEvents(), Conn.sqlAchemyConn())
    
if __name__ == "__main__":
    main()