import os
from normalize.normalize_json import Connection 
import pytest


def test_class_init_with_env_vars(monkeypatch):
    # GIVEN environment variables are set for host and port
    monkeypatch.setenv('SERVICE_HOST', 'localhost')
    monkeypatch.setenv('SERVICE_PORT', '3306')
    #   AND a class instance is initialized without specifying a host or port
    conn = Connection()
    # THEN the instance should reflect the host and port specified in the environment variables
    assert conn.host == 'localhost'
    assert conn.user == 'root'
    assert conn.passwd == 'root'
    assert conn.db == 'tamara'